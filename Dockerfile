FROM registry.ethz.ch/eduit/images/notebooks/24hs/jh-notebook-base:4.1.5-04

USER root

COPY start-singleuser.sh /usr/local/bin/

RUN mamba env create -f /builds/eduit/images/notebooks/24hs/jh-notebook-geospatial-analysis/environment.yml && \
  source activate cmga-env && \
  python -m ipykernel install --name=cmga-env --display-name='Geospatial Analysis'
  

USER 1000
